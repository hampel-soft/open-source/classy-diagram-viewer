﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="API" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Class To String.vi" Type="VI" URL="../API/Class To String.vi"/>
		<Item Name="Target To String.vi" Type="VI" URL="../API/Target To String.vi"/>
		<Item Name="Project To String.vi" Type="VI" URL="../API/Project To String.vi"/>
	</Item>
	<Item Name="classes" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="LV Project.lvclass" Type="LVClass" URL="../classes/LV Project/LV Project.lvclass"/>
		<Item Name="LV Target.lvclass" Type="LVClass" URL="../classes/LV Target/LV Target.lvclass"/>
		<Item Name="LV Class.lvclass" Type="LVClass" URL="../classes/LV Class/LV Class.lvclass"/>
		<Item Name="Class Field.lvclass" Type="LVClass" URL="../classes/Class Field/Class Field.lvclass"/>
		<Item Name="Class Item.lvclass" Type="LVClass" URL="../classes/Class Item/Class Item.lvclass"/>
		<Item Name="Class Method.lvclass" Type="LVClass" URL="../classes/Class Method/Class Method.lvclass"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Call Get Parent Interfaces.vi" Type="VI" URL="../sub/Call Get Parent Interfaces.vi"/>
		<Item Name="Call Get Type String.vi" Type="VI" URL="../sub/Call Get Type String.vi"/>
		<Item Name="Trim Class Name.vi" Type="VI" URL="../sub/Trim Class Name.vi"/>
	</Item>
	<Item Name="sub" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Add Interface Implementation.vi" Type="VI" URL="../sub/Add Interface Implementation.vi"/>
		<Item Name="Add Relations to String.vi" Type="VI" URL="../sub/Add Relations to String.vi"/>
		<Item Name="Build UML String.vi" Type="VI" URL="../sub/Build UML String.vi"/>
		<Item Name="Class Relationship to UML String.vi" Type="VI" URL="../sub/Class Relationship to UML String.vi"/>
		<Item Name="Remove Duplicates in 1D Array.vi" Type="VI" URL="../sub/Remove Duplicates in 1D Array.vi"/>
		<Item Name="Scope to UML String.vi" Type="VI" URL="../sub/Scope to UML String.vi"/>
	</Item>
	<Item Name="typedefs" Type="Folder">
		<Item Name="Class Relationship Type.ctl" Type="VI" URL="../typedefs/Class Relationship Type.ctl">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Display Parameters.ctl" Type="VI" URL="../typedefs/Display Parameters.ctl">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Target Output.ctl" Type="VI" URL="../typedefs/Target Output.ctl"/>
	</Item>
</Library>
